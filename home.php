<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <meta name="description" content="Baze is a front-end starter template">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">

    <meta property="og:url" content="">
    <meta property="og:title" content="">
    <meta property="og:site_name" content="">
    <meta property="og:description" content="">
    <meta property="og:image" content="">
    <meta property="og:type" content="website">
    <meta property="fb:app_id" content="">

    <meta name="twitter:card" content="">
    <meta name="twitter:site" content="">
    <meta name="twitter:description" content="">
    <meta name="twitter:title" content="">
    <meta name="twitter:image" content="">

    <link rel="apple-touch-icon" href="assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="assets/img/favicon.png">

    <title>front-end starter kit</title>

    <link rel="stylesheet" href="assets/css/main.css">
    <script src="assets/js/vendor/modernizr.min.js"></script>
</head>
<body>
    <!--[if lt IE 9]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    
    <header class="navbar">
        <div class="container">
            <div class="logo">
                <a href="#!">Company</a>
            </div>
            <nav class="navbar-links">
                <ul class="list-nostyle">
                    <l1 class="navbar-item navbar-dropdown" data-target="#aboutDropdown">
                        <a class="navbar-dropdown-button" href="#">About</a>
                        <div class="navbar-dropdown-content" id="aboutDropdown">
                            <ul class="list-nostyle">
                                <li><a href="#!">History</a></li>
                                <li><a href="#!">Vision Mission</a></li>
                            </ul>
                        </div>
                    </l1>
                    <li class="navbar-item"><a href="#">Our Work</a></li>
                    <li class="navbar-item"><a href="#">Our Team</a></li>
                    <li class="navbar-item"><a href="#">Contact</a></li>
                </ul>
            </nav>
            <button class="navbar-hamburger">&#9776;</button>
        </div>
    </header>

    <main>
        <div class="hero">
            <div class="carousel" data-slick='{"slidesToShow": 1, "slidesToScroll": 1}'>
                <figure class="carousel-item">
                    <img class="carousel-img" src="assets/img/about-bg.jpg" alt="">
                    <div class="container">
                        <figcaption class="carousel-caption">
                            We don't have the best but we have the greatest team
                        </figcaption>
                    </div>
                </figure>
                <figure class="carousel-item">
                    <img class="carousel-img" src="assets/img/bg.jpg" alt="">
                    <div class="container">
                        <figcaption class="carousel-caption">
                            This is a place where technology and creativity fused into digital chemistry
                        </figcaption>
                    </div>
                </figure>
            </div>
        </div>
        <div class="container">
            <section class="section">
                <h3 class="section-title">OUR VALUES</h3>
                <ul class="value-items list-nostyle">
                    <li class="value-item value-item--innovative">
                        <img class="value-img" src="assets/img/lightbulb-o.png" alt="">
                        <h4 class="value-title">Innovative</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam facilis reiciendis dolorem accusamus.</p>
                    </li>
                    <li class="value-item value-item--loyalty">
                        <img class="value-img" src="assets/img/bank.png" alt="">
                        <h4 class="value-title">Loyalty</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam facilis reiciendis dolorem accusamus.</p>
                    </li>
                    <li class="value-item value-item--respect">
                        <img class="value-img" src="assets/img/balance-scale.png" alt="">
                        <h4 class="value-title">Respect</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam facilis reiciendis dolorem accusamus.</p>
                    </li>
                </ul>
            </section>
            <section class="section">
                <h3 class="section-title">CONTACT US</h3>
                <form class="contact-form">
                    <div class="form__row">
                        <label class="form-label" for="contactName">Name</label>
                        <input class="form-input contact-input" type="text" name="name" id="contactName" required>
                    </div>
                    <div class="form__row">
                        <label class="form-label" for="contactMail">Email</label>
                        <input class="form-input contact-input" type="email" name="email" id="contactEmail" required>
                    </div>
                    <div class="form__row">
                        <label class="form-label" for="contactMessage">Message</label>
                        <textarea class="form-input contact-input" name="message" id="contactMessage" required=""></textarea>
                    </div>
                    <button class="btn contact-input" id="contactSubmit">SUBMIT</button>
                </form>
            </section>
        </div>
    </main>

    <footer class="footer">
        <p>Copyright &copy; 2016. PT Company</p>
        <ul class="list-nostyle">
            <li class="footer-item"><a href="#!"><img class="footer-img" src="assets/img/facebook-official.png" alt="facebook"></a></li>
            <li class="footer-item"><a href="#!"><img class="footer-img" src="assets/img/twitter.png" alt="twitter"></a></li>
        </ul>
    </footer>




    <script>window.myPrefix = '';</script>
    <script src="assets/js/vendor/jquery.min.js"></script>
    <script src="assets/js/vendor/slick.min.js"></script>
    <script src="assets/js/vendor/baze.validate.min.js"></script>
    <script src="https://cdn.polyfill.io/v2/polyfill.js?features=default,promise,fetch"></script>
    <script src="assets/js/main.min.js"></script>
</body>
</html>
