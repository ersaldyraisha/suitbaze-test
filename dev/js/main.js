/*! [PROJECT_NAME] | Suitmedia */

((window, document, undefined) => {

    const path = {
        css: `${myPrefix}assets/css/`,
        js : `${myPrefix}assets/js/vendor/`
    }

    const assets = {
        _objectFit      : `${path.js}object-fit-images.min.js`
    }

    const Site = {
        enableActiveStateMobile() {
            if ( document.addEventListener ) {
                document.addEventListener('touchstart', () => {}, true)
            }
        },

        WPViewportFix() {
            if ( '-ms-user-select' in document.documentElement.style && navigator.userAgent.match(/IEMobile/) ) {
                let style = document.createElement('style')
                let fix = document.createTextNode('@-ms-viewport{width:auto!important}')

                style.appendChild(fix)
                document.head.appendChild(style)
            }
        },

        objectFitPolyfill() {
            load(assets._objectFit).then( () => {
                objectFitImages()
            })
        },

        mobileDropdown(){
            let $navLinks = $('.navbar-links')

            $('.navbar-hamburger').on('click', e => {
                if ($navLinks.css('display') == 'block'){
                    $navLinks.css('display', 'none')
                } else {
                    $navLinks.css('display', 'block')
                }
            })
        },

        carousel() {
            $('.carousel').slick({
                dots: false,
                infinite: true,
                speed: 300,
                slidesToShow: 1,
                autoplay: false,
                prevArrow: '<button type="button" class="carousel-button carousel-button--prev"><img src="assets/img/left.png"></button>',
                nextArrow: '<button type="button" class="carousel-button carousel-button--next"><img src="assets/img/right.png"></button>'
            })
        },

        validate() {
            $('form').bazeValidate({
                classInvalid    : 'form-input--error',
                //classValid      : 'input-valid',
                classMsg        : 'msg-error',
                msgEmpty        : 'This field is required.',
                msgEmail        : 'Invalid email address.',
                msgNumber       : 'Input must be number.',
                msgExceedMin    : 'Minimum number is %s.',
                msgExceedMax    : 'Maximum number is %s.',
                onValidating    : null,
                onValidated     : null
            })
        }

        /* validate() {
            let $form = $('.contact-form')
            let $name = $('#contactName')
            let $mail = $('#contactEmail')
            let $message = $('#contactMessage')
            let $submit = $('#contactSubmit')
            
            $form.on('submit', e => {
                if(!validateName($name) && !validateMail($mail) && !validateMessage($message)){
                    console.log('form valid')
                } else {
                    e.preventDefault()
                    console.log('form invalid')
                }
            })

            $name.on('change', e => {
                validateName($(e.currentTarget))
            }).on('click', e => {
                validateName($(e.currentTarget))
            })

            $mail.on('change', e => {
                validateMail($(e.currentTarget))
            }).on('click', e => {
                validateMail($(e.currentTarget))
            })

            $message.on('change', e => {
                validateMessage($(e.currentTarget))
            }).on('click', e => {
                validateMessage($(e.currentTarget))
            })


            function validateName($this){
                return validateNull($this)
            }

            function validateMessage($this){
                return validateNull($this)
            }

            function validateMail($this){
                if (!validateNull($this)) {   
                    return validateMail($this)
                }

                return validateNull($this)
            }

            function validateNull($this){
                if ($this.val() == '') {
                    $this.addClass('form-input--error')
                    //console.log($this.attr('name') + ' inputed null')
                    return true
                } else {
                    $this.removeClass('form-input--error')
                    return false
                }
            }

            function validateMail($this){
                let regExp = /\S+@\S+\.\S+/
                
                if ($this.val().match(regExp) == null){
                    $this.addClass('form-input--error')
                    //console.log('mail invalid: ' + $this.val().match(regExp))
                    return true
                } else {
                    $this.removeClass('form-input--error')
                    return false
                } 
            }
        } */
    }

    Promise.all([
        
    ]).then(() => {
        for (let fn in Site) {
            Site[fn]()
        }
        window.Site = Site
    })

    function exist(selector) {
        return new Promise((resolve, reject) => {
            let $elem = $(selector)

            if ( $elem.length ) {
                resolve($elem)
            } else {
                reject(`no element found for ${selector}`)
            }
        })
    }

    function load(url) {
        return new Promise((resolve, reject) => {
            Modernizr.load({
                load: url,
                complete: resolve
            })
        })
    }

    function loadJSON(url) {
        return new Promise((resolve, reject) => {
            fetch(url).then(res => {
                if ( res.ok ) {
                    resolve(res.json())
                } else {
                    reject('Network response not ok')
                }
            }).catch(e => {
                reject(e)
            })
        })
    }

})(window, document)

